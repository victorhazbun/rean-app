'use strict';

require('dotenv').config();

// Get dependencies
const express = require('express');
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');
const rdb = require('rethinkdb');
const dbConfig = require('./config/database');

// Get our API routes
const api = require('./server/routes/api');

const app = express();

// Parsers for POST data
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// Point static path to dist
app.use(express.static(path.join(__dirname, 'dist')));

// Set our api routes
app.use('/api', api);

// Catch all other routes and return the index file
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'dist/index.html'));
});

/**
 * Get port from environment and store in Express.
 */
const port = process.env.PORT || '3000';
app.set('port', port);

/**
 * Create HTTP server.
 */
const server = http.createServer(app);
const io = require('socket.io')(server);

/**
 * Listen on provided port, on all network interfaces.
 */
server.listen(port, () => console.log(`API running on localhost:${port}`));

rdb.connect(dbConfig).then(function (connection) {
  io.on('connection', function (socket) {
    rdb.table('posts').changes({includeInitial: true}).run(connection, function(err, cursor) {
      if (err) { throw err; }
      cursor.each(function(err, row) {
        if (err) { throw err; }
        socket.emit('post', row.new_val);
      });
    });
  });
});

module.export = {server: server};