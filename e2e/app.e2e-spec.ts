import { ReanAppPage } from './app.po';

describe('rean-app App', function() {
  let page: ReanAppPage;

  beforeEach(() => {
    page = new ReanAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
