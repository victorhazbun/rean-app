'use strict';

const express = require('express');
const router = express.Router();
var rdb = require('../../lib/rethink');

/* GET api listing. */
router.get('/', (req, res) => {
  res.send('api works');
});

router.get('/posts', (req, res) => {

  rdb.findAll('posts')
    .then(posts => {
      res.json(posts);
    });
});

module.exports = router;