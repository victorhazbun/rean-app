import { Component, OnInit } from '@angular/core';
import * as io from 'socket.io-client';

export interface Post {
  title: string;
  body: string;
}

@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {
  // instantiate posts to an empty array
  posts: Array<Post>;

  constructor() { }

  ngOnInit() {
    // Retrieve posts from the API
    this.posts = [];
    io.connect().on('post', post => {
      this.posts.push(post);
    });
  }
}
